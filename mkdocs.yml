# A human-friendly name for the site as a whole.
site_name: 'Raven Documentation'
site_url: 'https://docs.raven.cam.ac.uk/'

copyright: The Raven Authentication Service is made with ❤ by <a href="https://guidebook.devops.uis.cam.ac.uk/en/latest/">UIS DevOps</a>

# Site layout
nav:
  - 'index.md'
  - 'overview.md'
  - 'Quickstarts':
    - 'first-steps.md'
    - 'wordpress.md'
    - 'apache-oauth2.md'
    - 'apache-saml2.md'
  - 'Guides':
    - 'golden-rules.md'
    - 'raven-oauth2.md'
    - 'creating-oauth2-client-credentials.md'
    - 'raven-saml2.md'
    - 'lookup.md'
  - 'Developing Applications':
    - 'develop.md'
    - 'django-oauth2.md'
  - 'Reference':
    - 'oauth2-claims.md'
    - 'saml2-attributes.md'
    - 'reference-apache-oauth2.md'
    - 'reference-apache-saml2.md'
    - 'firewall-configuration.md'

# Links for sufficiently with it people to submit changes.
repo_url: 'https://gitlab.developers.cam.ac.uk/uis/devops/raven/docs/'
edit_uri: 'blob/master/docs'
repo_name: 'Developer Hub Project'

# Make us look pretty.
theme:
  name: 'material'
  logo: 'images/logo.svg'
  favicon: 'images/favicon.png'
  palette:
    primary: 'indigo'
    secondary: 'amber'

# Custom styling
extra_css:
  - 'css/extra.css'

markdown_extensions:
  # Support per-page extended metadata
  - meta

  # Support notes, warnings, etc.
  - admonition

  # Allow the use of Pygments to do code highlighting. Do not attempt to guess
  # the language if we don't specify.
  - codehilite:
      guess_lang: false

  # Provide permalinks to help with linking to sections.
  - toc:
      permalink: true

  # Support GitLab/GitHub-style task lists.
  - pymdownx.tasklist:
      custom_checkbox: true

  # Link-ify URLs.
  - pymdownx.magiclink

  # Inline code highligheing.
  - pymdownx.inlinehilite

  # Allow code blocks to be nested inside other elements
  - pymdownx.superfences

  # Allow external code inclusion
  - pymdownx.snippets

plugins:
  - search
  - minify:
      minify_html: true
  - git-revision-date-localized
