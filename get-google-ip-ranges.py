#!/usr/bin/env python3
import ipaddress
import json
from urllib.request import urlopen

# All of Google's announced IP networks.
with urlopen('https://www.gstatic.com/ipranges/goog.json') as response:
    all_networks = [
        ipaddress.ip_network(
            prefix_obj.get('ipv4Prefix', prefix_obj.get('ipv6Prefix'))
        )
        for prefix_obj in json.load(response)['prefixes']
    ]

# Those which can be used by third parties.
with urlopen('https://www.gstatic.com/ipranges/cloud.json') as response:
    customer_networks = [
        ipaddress.ip_network(
            prefix_obj.get('ipv4Prefix', prefix_obj.get('ipv6Prefix'))
        )
        for prefix_obj in json.load(response)['prefixes']
    ]

# Compute "all" - "customer" networks.
remainder_networks = all_networks
for n1 in customer_networks:
    new_remainder_networks = []
    for n2 in remainder_networks:
        if n1.version == n2.version and n1.overlaps(n2):
            new_remainder_networks.extend(n2.address_exclude(n1))
        else:
            new_remainder_networks.append(n2)
    remainder_networks = new_remainder_networks

# Show remaining networks.
for network in remainder_networks:
    print(network)
