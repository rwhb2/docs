title: OAuth2

# Generic Raven OAuth2 instructions

Raven OAuth2 is provided by Google and conforms to the [OpenID
Connect](https://openid.net/connect/) specification. Any application supporting
sign in with Google can make use of Raven OAuth2. For detailed instructions on
adding Google sign in support see the following Google documentation pages:

* [OAuth2 for Web Server
    Applications](https://developers.google.com/identity/protocols/OAuth2WebServer)
* [OAuth2 for Client-side Web
    Applications](https://developers.google.com/identity/protocols/OAuth2UserAgent)
* [OAuth2 for Mobile & Desktop
    Apps](https://developers.google.com/identity/protocols/OAuth2InstalledApp)

!!! important

    The user experience for Raven OAuth2 may differ from the user experience
    shown by legacy Raven services. In particular users will be asked for
    consent on the first sign in. Please see the [notice to IT
    professionals](https://help.uis.cam.ac.uk/service/support/help-for-institutions/google-cloud-identity)
    on the UIS website for more information.

## Registering OAuth2 client credentials

On order to make use of Raven OAuth2 you will need to register your application.
There are [instructions on registering an
application](creating-oauth2-client-credentials.md) elsewhere in this
documentation.

## OAuth2 client configuration

This documentation covers configuring [wordpress](wordpress.md) and the [Apache
web-server](apache-oauth2.md) for Raven OAuth2. There are many OAuth2-capable
applications and we cannot provide detailed instructions for all of them.

Some of the configuration parameters you may find are listed below along with
the values you should use with Raven OAuth2.

|Parameter|Value|
|-|-|
|OIDC Metadata URL|`https://accounts.google.com/.well-known/openid-configuration`|
|Scopes|`profile email openid`|
|Login Endpoint URL|`https://accounts.google.com/o/oauth2/v2/auth?hd=cam.ac.uk`|
|Userinfo Endpoint URL|`https://openidconnect.googleapis.com/v1/userinfo`|
|Token Validation Endpoint URL|`https://oauth2.googleapis.com/token`|
|Revocation Endpoint URL|`https://oauth2.googleapis.com/revoke`|
|Display Name Claim|`name`|
|Username or Email Claim|`email`|
|JWKS Certificates|`https://www.googleapis.com/oauth2/v3/certs`|
