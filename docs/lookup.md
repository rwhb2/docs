title: Using the Lookup directory

# Using the Lookup directory with Apache

!!! important
    Currently, Lookup can only be accessed by servers which are hosted on
    machines within the University network.

In the [Raven "golden rules"](golden-rules.md) we noted that the University's
[Lookup directory](https://www.lookup.cam.ac.uk/) should be used as an
authoritative source of information on users.

Lookup has a [LDAP
personality](https://help.uis.cam.ac.uk/service/collaboration/lookup/ldapqueries)
which can be used to retrieve information on University members. Many
applications will have native support for LDAP. We can't cover them all here.
This page discusses using the LDAP personality of Lookup with Apache.

!!! note
    The Lookup LDAP personality is only currently available within the CUDN. For
    Cloud-hosted applications you may need to use a VPN or another LDAP server.

The
[mod_authnz_ldap](https://httpd.apache.org/docs/2.4/mod/mod_authnz_ldap.html)
module can be used to add LDAP support to an Apache web-server. This module is
usually available via an Operating System package. For example, on Debian or
Ubuntu machines the module is included with the basic Apache install and can be
enabled via the following commands:

```bash
a2enmod authnz_ldap

a2enmod ldap
```

!!! important
    The mod_ldap module used by mod_authnz_ldap includes a cache. This means
    that changes in Lookup will not immediately be reflected on your site. See
    the [mod_ldap
    documentation](https://httpd.apache.org/docs/2.4/mod/mod_ldap.html) for
    information on how to tune the caching.

You *must* ensure that all connections between Apache are secured using TLS
(Transport Layer Security). On Debian or Ubuntu systems this can be achieved by
adding the following to `/etc/apache2/mods-enabled/ldap.conf`:

```apache
LDAPTrustedMode TLS
```

The examples on this page assume you are using Raven OAuth2. We also assume
you've used the `#!apache OIDCRemoteUserClaim` parameter to extract the local
part of the user's email address within the `#!apache <VirtualServer>` section:

```apache
OIDCRemoteUserClaim email "^(.*)@"
```

!!! danger
    By using Lookup as the only user directory and splitting off the domain-part
    of the user's email address we are restricting ourselves to users who have
    CRSids. As noted in the [Raven "golden rules"](golden-rules.md) this will
    make it hard for you to add additional users to your site in future.

## Allowing only members of an institution or institutions

Within a `#!apache VirtualServer` section include the following:

```apache
<Location />
    AuthType openid-connect

    <RequireAll>
        Require valid-user
        Require claim "hd:cam.ac.uk"

        <RequireAny>
          AuthLDAPUrl ldap://ldap.lookup.cam.ac.uk/ou=people,o=University%20of%20Cambridge,dc=cam,dc=ac,dc=uk?uid

          # List Lookup "instid"s for each allowed institution
          Require ldap-attribute instID=UIS
          Require ldap-attribute instID=CL
        </RequireAny>
    </RequireAll>
</Location>
```

## Allowing only a whitelisted set of users

Within a `#!apache VirtualServer` section include the following:

```apache
<Location />
    AuthType openid-connect

    <RequireAll>
        Require valid-user
        Require claim "hd:cam.ac.uk"

        <RequireAny>
          AuthLDAPUrl ldap://ldap.lookup.cam.ac.uk/ou=people,o=University%20of%20Cambridge,dc=cam,dc=ac,dc=uk?uid

          # List users separated with whitespace
          Require ldap-user spqr1 fjc55
        </RequireAny>
    </RequireAll>
</Location>
```

## Allowing only members of a Lookup group or groups

Within a `#!apache VirtualServer` section include the following:

```apache
<Location />
    AuthType openid-connect

    <RequireAll>
        Require valid-user
        Require claim "hd:cam.ac.uk"

        <RequireAny>
          AuthLDAPUrl ldap://ldap.lookup.cam.ac.uk/ou=people,o=University%20of%20Cambridge,dc=cam,dc=ac,dc=uk?uid

          # List Lookup "groupid"s for each allowed group. Use "groupName"
          # instead of "groupID" to use the group short name. Using the short
          # name is not robust against groups being renamed.
          Require ldap-attribute groupID=101855
          Require ldap-attribute groupID=101611
        </RequireAny>
    </RequireAll>
</Location>
```

## More complex queries

The [ldap-filter
directive](https://httpd.apache.org/docs/2.4/mod/mod_authnz_ldap.html#reqfilter)
can be used to form more complex queries specifying who can access a site and is
a good choice if you need to support non-`@cam.ac.uk` users on your site.
