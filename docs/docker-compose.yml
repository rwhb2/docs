version: '3.6'

# A docker compose "service" defines an individual running container. To learn
# more about Docker and containers in general, visit
# https://docs.docker.com/get-started/.

services:
  # This container runs the site we want to protect. In production this site is
  # open to all on the internal network but is firewalled off from the public
  # Internet. We simulate this in our docker-compose configuration by creating a
  # private network called "internal" which this site appears on.
  #
  # Within their networks, docker-compose services are available via their
  # service name so, on the "internal" network, this website is available at
  # http://my-site/.
  my-site:
    # Spin up a test web server which simply echos any request it gets.
    image: containous/whoami

    # Web servers usually run on port 80.
    expose:
      - 80

    networks:
      - internal

  # This container runs a web server which will proxy incoming requests from the
  # public Internet to the protected site.
  proxy:
    # Use a custom image based on Apache.
    build:
      context: ../containers/apache-proxy/

    # The nginx server runs on port 80. The "ports" directive tells
    # docker-compose to make this port available on the host using port 80.
    ports:
      - '8080:80'

    # Environment variables are used to configure the proxy. Their values are
    # interpolated into the Apache configuration.
    environment:
      CLIENT_ID: ${CLIENT_ID}
      CLIENT_SECRET: ${CLIENT_SECRET}
      BACKEND_URL: http://my-site/
      OIDC_CRYPTO_PASSPHRASE: this-should-be-some-long-secret

    # We add the service to the internal network so that it can "see"
    # http://my-site/.
    networks:
      - internal

# Docker-compose requires that all networks used are named in a top-level
# "networks" block. We only use one network in this configuration. List it here.
networks:
  internal:
